#!/bin/sh
#
# $Id: lan.sh,v 1.27 2010-12-03 09:50:18 winfred Exp $
#
# usage: wan.sh
#

. /sbin/global.sh
. /etc/init.d/vstfunc

if [ "$1" = "b" ] && [ -f /etc/hdelay ]; then
	exit 0
fi
# stop all
#killproc udhcpd
#killproc igmpproxy
#killall -q upnpd
#killall -q radvd
#killall -q pppoe-relay
#killall -q dnsmasq
#rm -rf /var/run/lld2d-*
#echo "" > /var/udhcpd.leases

# ip address
ip=`nvram_get 2860 lan_ipaddr`
nm=`nvram_get 2860 lan_netmask`
ifconfig $lan_if down
ifconfig $lan_if $ip netmask $nm
opmode=`nvram_get 2860 OperationMode`
if [ "$opmode" = "0" ]; then
	gw=`nvram_get 2860 wan_gateway`
	pd=`nvram_get 2860 wan_primary_dns`
	sd=`nvram_get 2860 wan_secondary_dns`
	route del default
	route add default gw $gw
	config-dns.sh $pd $sd
fi

#ifconfig "br0:9" down
#ifconfig "eth2:9" down
#lan2enabled=`nvram_get 2860 Lan2Enabled`
#if [ "$lan2enabled" = "1" ]; then
#	ip_2=`nvram_get 2860 lan2_ipaddr`
#	nm_2=`nvram_get 2860 lan2_netmask`
#	if [ "$opmode" = "0" ]; then
#		ifconfig "br0:9" "$ip_2" netmask "$nm_2"
#
#	elif [ "$opmode" = "1" ]; then
#		ifconfig "br0:9" "$ip_2" netmask "$nm_2"
#		echo "ifconfig "br0:9" "$ip_2" netmask "$nm_2""
#	elif [ "$opmode" = "2" ]; then
#		ifconfig "eth2:9" "$ip_2" netmask "$nm_2"
#		echo "ifconfig "eth2:9" "$ip_2" netmask "$nm_2""
#	elif [ "$opmode" = "3" ]; then
#		ifconfig "br0:9" "$ip_2" netmask "$nm_2"
#		echo "ifconfig "br0:9" "$ip_2" netmask "$nm_2""
#	fi
#fi

# hostname
#host=`nvram_get 2860 HostName`
#if [ "$host" = "" ]; then
#	host="ralink"
#	nvram_set 2860 HostName ralink
#fi
#hostname $host
#echo "127.0.0.1 localhost.localdomain localhost" > /etc/hosts
#echo "$ip $host.ralinktech.com $host" >> /etc/hosts

# dhcp server
/etc/init.d/dhcpd.sh restart
pioctl wifi 0

#pd=`nvram_get 2860 dhcpPriDns`
#sd=`nvram_get 2860 dhcpSecDns`
if [ "$wanmode" = "STATIC" ]; then
	pd=`nvram_get 2860 wan_primary_dns`
	sd=`nvram_get 2860 wan_secondary_dns`
		if [ -n $pd ]; then
			config-staticdns.sh "$pd"
		elif [ -n $sd ]; then
			config-staticdns.sh "$sd"
		fi

else
while read line;do
	pd=`echo $line | grep nameserver |awk -F ' ' '{printf $2}'`
	if [ -n $pd ]; then
		break
	fi
done < /etc/resolv.conf

if [ -n $pd ]; then
	config-staticdns.sh $pd
fi
fi
# lltd
#lltd=`nvram_get 2860 lltdEnabled`
#if [ "$lltd" = "1" ]; then
#	lld2d $lan_if
#fi

# igmpproxy
#igmp=`nvram_get 2860 igmpEnabled`
#if [ "$igmp" = "1" ]; then
#	config-igmpproxy.sh $wan_if $lan_if
#fi

# upnp
#if [ "$opmode" = "0" -o "$opmode" = "1" ]; then
#	upnp=`nvram_get 2860 upnpEnabled`
#	if [ "$upnp" = "1" ]; then
#		route add -net 239.0.0.0 netmask 255.0.0.0 dev $lan_if
#		upnp_xml.sh $ip
#		upnpd -f $wan_ppp_if $lan_if &
#	fi
#fi

# radvd
#radvd=`nvram_get 2860 radvdEnabled`
#ifconfig sit0 down
#echo "0" > /proc/sys/net/ipv6/conf/all/forwarding
#if [ "$radvd" = "1" ]; then
#	echo "1" > /proc/sys/net/ipv6/conf/all/forwarding
#	ifconfig sit0 up
#	ifconfig sit0 add 2002:1101:101::1101:101/16
#	route -A inet6 add 2000::/3 gw ::17.1.1.20 dev sit0
#	route -A inet6 add 2002:1101:101:0::/64 dev br0
#	radvd -C /etc_ro/radvd.conf -d 1 &
#fi

# pppoe-relay
#pppr=`nvram_get 2860 pppoeREnabled`
#if [ "$pppr" = "1" ]; then
#	pppoe-relay -S $wan_if -B $lan_if
#fi

# dns proxy
#dnsp=`nvram_get 2860 dnsPEnabled`
#if [ "$dnsp" = "1" ]; then
#	dnsmasq &
#fi

