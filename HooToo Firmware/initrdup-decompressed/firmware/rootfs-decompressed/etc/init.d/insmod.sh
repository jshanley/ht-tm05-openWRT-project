#!/bin/sh
#*************************文件系统***********************
insmod_filesys_modules(){
	if [ -f lib/modules/2.6.21/kernel/drivers/tuxera/tntfs.ko ]; then
        	insmod lib/modules/2.6.21/kernel/drivers/tuxera/tntfs.ko
	fi
	if [ -f lib/modules/2.6.21/kernel/drivers/tuxera/texfat.ko ]; then
       		insmod lib/modules/2.6.21/kernel/drivers/tuxera/texfat.ko
	fi
	if [ -f lib/modules/2.6.21/kernel/drivers/tuxera/thfsplus.ko ]; then
       		insmod lib/modules/2.6.21/kernel/drivers/tuxera/thfsplus.ko
	fi
	if [ -f lib/modules/2.6.21/kernel/drivers/tuxera/tfat.ko ]; then
       		insmod lib/modules/2.6.21/kernel/drivers/tuxera/tfat.ko
	fi
}

if [ -f lib/modules/2.6.21/kernel/drivers/net/MT7610_ap.ko ]; then
       insmod lib/modules/2.6.21/kernel/drivers/net/MT7610_ap.ko
fi
if [ -f lib/modules/wireless-sohu-2014-09-28-17-13.ko ]; then
       insmod lib/modules/wireless-sohu-2014-09-28-17-13.ko
fi

#******* add by wuliang ***** 3G功能 *********************
insmod_3g_fun_modules(){
	if [ -f lib/modules/sohu_huawei3G_cdc_encap.ko ]; then
       		insmod lib/modules/sohu_huawei3G_cdc_encap.ko
	fi
	if [ -f lib/modules/sohu_huawei3G_usbnet.ko ]; then
       		insmod lib/modules/sohu_huawei3G_usbnet.ko
	fi
	if [ -f lib/modules/sohu_huawei3G_cdc_ether.ko ]; then
       		insmod lib/modules/sohu_huawei3G_cdc_ether.ko
	fi
}

#*****************************ip 设置*********************
insmod_ip_set_modules(){
	[ -f /lib/modules/iptable_mangle.ko ] && insmod /lib/modules/iptable_mangle.ko
	[ -f /lib/modules/nfnetlink.ko ] && insmod /lib/modules/nfnetlink.ko
	[ -f /lib/modules/nfnetlink_queue.ko ] && insmod /lib/modules/nfnetlink_queue.ko
	[ -f /lib/modules/nf_conntrack_netlink.ko ] && insmod /lib/modules/nf_conntrack_netlink.ko
	[ -f /lib/modules/xt_NFQUEUE.ko ] && insmod /lib/modules/xt_NFQUEUE.ko
	[ -f /lib/modules/xt_SECMARK.ko ] && insmod /lib/modules/xt_SECMARK.ko
	[ -f /lib/modules/xt_hashlimit.ko ] && insmod /lib/modules/xt_hashlimit.ko
	[ -f /lib/modules/xt_mark.ko ] && insmod /lib/modules/xt_mark.ko
	[ -f /lib/modules/xt_connmark.ko ] && insmod /lib/modules/xt_connmark.ko
	[ -f /lib/modules/ipt_REJECT.ko ] && insmod /lib/modules/ipt_REJECT.ko

	[ -f /lib/modules/ip_set.ko ] && insmod /lib/modules/ip_set.ko
	[ -f /lib/modules/ip_set_hash_ip.ko ] && insmod /lib/modules/ip_set_hash_ip.ko
	[ -f /lib/modules/xt_set.ko ] && insmod /lib/modules/xt_set.ko
}

#***********************************************************
insmod_mbcache_modules(){
	[ -f /lib/modules/2.6.21/kernel/drivers/mbcache.ko ] && [ -f /lib/modules/2.6.21/kernel/drivers/jbd2.ko ] && [ -f /lib/modules/2.6.21/kernel/drivers/ext4.ko ] && (
	insmod /lib/modules/2.6.21/kernel/drivers/mbcache.ko
	insmod /lib/modules/2.6.21/kernel/drivers/jbd2.ko
	insmod /lib/modules/2.6.21/kernel/drivers/ext4.ko
	)
}
insmod_filesys_modules
insmod_3g_fun_modules
insmod_ip_set_modules
insmod_mbcache_modules

echo 1 > /proc/sys/vm/drop_caches
if [ -f /etc/hdelay ]; then
	/etc/init.d/udev  > /dev/null 2>&1
fi
