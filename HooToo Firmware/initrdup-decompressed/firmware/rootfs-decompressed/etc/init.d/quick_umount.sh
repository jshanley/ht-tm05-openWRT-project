#!/bin/sh
#quick umount all disk

kill_all_process()
{
        echo "Stop all app ..."
	sync
	sync
        SELF_PID=$$
        ps aux > /tmp/ps.log

        WDOG_PID=`pidof watchdog`
        [ -n "$WDOG_PID" ] && diswatchdog && sleep 1 && WPID=`pidof watchdog`

        while read USER       PID CPU MEM    VSZ   RSS TTY      STAT START   TIME COMMAN
        do
                [  $PID == 1 ] && continue
                [ "$SELF_PID" == "$PID" ] && continue
                [ "$TTY" != '?' ] && continue
                [ -n "$WPID" ] && [ "$SELF_PID" == "$PID" ] && continue

                kill -9 $PID 1>/dev/null 2>&1

        done < /tmp/ps.log
}

close_swap(){
	swapfile_path="$(grep swapfile /proc/swaps | awk '{print $1}')"
	[ -n "$swapfile_path" ] && (
		swapoff "$swapfile_path"
	) 
}

qiuck_umount(){
	while read dev mountpoit ftype option ; do
		[ -n "$(echo "$mountpoit" | grep data)" ] && (
			umount "$mountpoit"
			[ "$?" != "0" ] && umount2 "$mountpoit"
		) 
	done < /proc/mounts
}

disk_sleep_control(){
        while read major minor blocks name; do
                [ -n "$(echo $name | grep sd)" ] && [ ${#name} -eq 3 ] && {
                        /usr/sbin/sg_ioctl_disk /dev/$name 0
                }
        done < /proc/partitions
}

kill_all_process
close_swap
qiuck_umount
disk_sleep_control

case "$1" in
        reboot)
		echo a > /proc/vs_reboot_led
                reboot -f
                ;;
        shutdown)
                echo 0 > /proc/vs_gpio_power_off
                ;;
        *)
                echo "Begin kill ps and unmount disk,but no reboot or shutdown!"
                ;;
esac

